package Math;

import java.util.ArrayList;

public class Matrix {
	private int rows;
	private int cols;
	private ArrayList<ArrayList<Double>> arr;

	public Matrix(int rows, int cols) {// Construtor que gera uma matrix com tamanho desejado preenchida por zeros
		this.rows = rows;
		this.cols = cols;

		arr = new ArrayList<>();
		for (int i = 0; i < rows; i++) {
			arr.add(new ArrayList<Double>());
			for (int j = 0; j < cols; j++) {
				arr.get(i).add(0.0);
			}
		}
	}

	public Matrix() {
		this.rows = 0;
		this.cols = 0;
		arr = new ArrayList<>();
	}

	public void ArrayToMatrix(double[] inA) {// metodo que converte a matris inA para MATRIX
		arr.clear();
		this.rows = 1;
		this.cols = inA.length;
		for (int i = 0; i < rows; i++) {
			arr.add(new ArrayList<Double>());
			for (int j = 0; j < cols; j++) {
				arr.get(i).add(inA[j]);
			}
		}
	}

	public void ArrayToMatrix(Double[][] inA) {// metodo que converte a matris inA para MATRIX

		arr.clear();
		this.cols = inA[0].length;
		this.rows = inA.length;
		for (int i = 0; i < rows; i++) {
			arr.add(new ArrayList<Double>());
			for (int j = 0; j < cols; j++) {
				arr.get(i).add(inA[i][j]);
			}
		}
	}

	public void ArrayToMatrix(double[][] inA) {// metodo que converte a matris inA para MATRIX

		arr.clear();
		this.cols = inA[0].length;
		this.rows = inA.length;
		for (int i = 0; i < rows; i++) {
			arr.add(new ArrayList<Double>());
			for (int j = 0; j < cols; j++) {
				arr.get(i).add(inA[i][j]);
			}
		}
	}

	public void Hadamard(Matrix inA, Matrix inB) {// metodo que faz a multiplicaçao termo a termo (obs: nao é
													// multiplicaçao matricial)

		Matrix Mresorse = new Matrix(inA.Size(0), inA.Size(1));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int j = 0; j < inA.Size(1); j++) {
				Mresorse.Alter(i, j, inA.Get(i, j) * inB.Get(i, j));
			}
		}
		Replace(Mresorse);
	}

	public void Randomize() {// coloca valors aleatorios na matrz
		for (int i = 0; i < Size(0); i++) {
			for (int j = 0; j < Size(1); j++) {
				Alter(i, j, Math.random() * 2 - 1);
//				Alter(i, j, 1);
			}
		}

	}

	public void SOFTMAX() {// metodo que aplica a funçao softmax
		double resorse = 0;
		for (int i = 0; i < Size(0); i++) {
			resorse = Math.exp(Get(i, 0)) + resorse;
		}
		for (int i = 0; i < Size(0); i++) {
			Alter(i, 0, Math.exp(Get(i, 0)) / resorse);
		}
	}

	public void Replace(Matrix inA) {// subistitui uma matriz b=inA;
		this.rows = inA.Size(0);
		this.cols = inA.Size(1);
		arr.clear();
		for (int i = 0; i < rows; i++) {
			arr.add(new ArrayList<Double>());
			for (int j = 0; j < cols; j++) {
				arr.get(i).add(inA.Get(i, j));
			}
		}
	}

	public void Transpose(Matrix inA) {// metodo responsavel por transpor a matrix
		Matrix Mresorse = new Matrix(inA.Size(1), inA.Size(0));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int j = 0; j < inA.Size(1); j++) {
				Mresorse.Alter(j, i, inA.Get(i, j));
			}
		}

		Replace(Mresorse);
	}

	public int Size(int i) {// retorna o numero de linha(Size(0)) e de colunas(Size(1))
		if (i == 0)
			return this.rows;
		else if (i == 1)
			return this.cols;
		else
			return 0;

	}

	public double Get(int rows, int cols) {
		// metodo que retorna o valor da matriz correspondente a linha e a cololuna
		// desejada
		return arr.get(rows).get(cols);
	}

	public void Alter(int rows, int cols, double valor) {
		// metodo que altera o valor da matriz desejada
		arr.get(rows).set(cols, valor);
	}

	public void PrintTable() {
		// metodo que printa a matriz
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				System.out.print(" " + Get(i, j));
			}
			System.out.print("\n");
		}
		System.out.print("\n-----------------------------\n");

	}

	public void Aplicafunc(ArrayList<Double> func) {// metodo que aplica as funçoes de ativaçao
		if (func.get(0) == TYPE.SOFTMAX)
			SOFTMAX();
		else {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					Alter(i, j, func(Get(i, j), func.get(j)));
				}
			}
		}
	}

	private Double func(Double x, Double func) {// aqui tem o calculo das funçoes de ativaçao
		if (func == TYPE.SIGMOID)
			return (1.0 / (1.0 + Math.exp(-x)));
		else if (func == TYPE.TANH)
			return ((2 / (1 + Math.exp(-2 * x))) - 1);
		else if (func == TYPE.ReLU)
			return Math.max(0, x);
		else if (func == TYPE.LeakyReLU)
			return Math.max(TYPE.alfalr * x, x);
		else if (func == TYPE.ELU) {
			if (x < 0)
				return (TYPE.alfaelu * (Math.exp(x) - 1));
			else
				return x;
		} else
			return 0.0;

	}

	public void Deriva(ArrayList<Double> func) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Alter(i, j, dervfunc(Get(i, j), func.get(j)));
			}
		}
	}

	public Double dervfunc(Double x, Double func) {
		if (func == TYPE.SIGMOID)
			return (x * (1.0 - x));
		else if (func == TYPE.TANH)
			return (1 - Math.pow(func(x, TYPE.TANH), 2));
		else if (func == TYPE.ReLU) {
			if (x < 0)
				return 0.0;
			else
				return 1.0;
		} else if (func == TYPE.LeakyReLU) {
			if (x < 0)
				return TYPE.alfalr;
			else
				return 1.0;
		} else if (func == TYPE.ELU) {
			if (x < 0)
				return (func(x, TYPE.ELU) + TYPE.alfaelu);
			else
				return 1.0;
		} else
			return 0.0;

	}

	public void Sum(Matrix inA, Matrix inB) {
//		metodo que soma duas matizes
		Matrix Mresorse = new Matrix(inA.Size(0), inA.Size(1));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int j = 0; j < inA.Size(1); j++) {
				Mresorse.Alter(i, j, inA.Get(i, j) + inB.Get(i, j));
			}
		}
		Replace(Mresorse);
	}

	public void Sub(Matrix inA, Matrix inB) {
//		metodo que subtrai duas matizes
		Matrix Mresorse = new Matrix(inA.Size(0), inA.Size(1));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int j = 0; j < inA.Size(1); j++) {
				Mresorse.Alter(i, j, inA.Get(i, j) - inB.Get(i, j));
			}
		}
		Replace(Mresorse);

	}

	public void Mult(Matrix inA, Matrix inB) {
//			metodo que faz a multiplicaçao matricial entre inA e inB
		Matrix Mresorse = new Matrix(inA.Size(0), inB.Size(1));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int k = 0; k < inB.Size(1); k++) {
				Mresorse.Alter(i, k, 0);
				for (int j = 0; j < inA.Size(1); j++)
					Mresorse.Alter(i, k, (inA.Get(i, j) * inB.Get(j, k)) + Mresorse.Get(i, k));
			}

		}
		Replace(Mresorse);
	}

	public void Mult(Matrix inA, double escalar) {
//		metodo que multiplica uma matizes x constante
		Matrix Mresorse = new Matrix(inA.Size(0), inA.Size(1));
		for (int i = 0; i < inA.Size(0); i++) {
			for (int j = 0; j < inA.Size(1); j++) {
				Mresorse.Alter(i, j, inA.Get(i, j) * escalar);
			}

		}

		Replace(Mresorse);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Matrix A = new Matrix(2, 2);
		Matrix B = new Matrix(2, 2);
		A.Transpose(B);
		A.Transpose(A);
//		A=B(t);
	}
}
