package Case;

public class Modelo {
	private double[] bias = { -1.621030911813572, 0.8358530064963875, -0.15308000977426162, 0.8590191637486585,
			0.7792468200484539, -0.3177905984142155, 0.872845630440547 };
	private double[][] weights = { { 7.07149638833484, 7.640085010308282, 5.991785447725505 },
			{ -4.2226896207158084, -2.696089543489598, -2.177714884708949 },
			{ 7.972988052460948, -3.400622246027987, -2.360354980029332 },
			{ -6.592477136524468, 1.8625264902071996, 0.11283227787374021 },
			{ -4.7892694617662, -0.021007299931148932, 4.534395101973254 },
			{ 7.687357736760569, 2.1380570625861526, -2.2114633749255477 },
			{ -6.561223265499364, -5.020015919208999, -3.3531603721289205 } };

	public int Cal(double[] RGB) {
		double cal = 0;
		double maior = 0;
		int cor = 0;
		for (int j = 0; j < bias.length; j++) {
			for (int i = 0; i < weights[0].length; i++) {
				cal = RGB[i] * weights[j][i];
			}
			cal = cal + bias[j];
			cal = Math.exp(cal);
			if (cal > maior) {
				maior = cal;
				cor = j;
			}
		}

		return (cor+1);

	}

	public static void main(String[] args) {
		Modelo M = new Modelo();
		double RGB[] = { 0.009803922, 0.023529412, 0.03627451 };
		System.out.println("cor: " + M.Cal(RGB));

	}

}
