package Case;

import Data.DataBase;
import Marchine_learning.RedeNeural;
import Math.Matrix;
import Math.TYPE;

public class Using {
	private RedeNeural RN;
	private DataBase Data;

	Using(int n_input, int n_output, Double[][] FuncAtiva) {
		Data = new DataBase(n_input, n_output);
		RN = new RedeNeural(3, FuncAtiva);

	}

	

	private double[] Dataout(int index) {
		double out[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } ;
		Double cor = Data.GetOut(index)[0] - 1.0;
		out[cor.intValue()] = 1.0;
		return out;
	}

	public void Train() {
		Matrix resposta = new Matrix();
		double maior = 0;
		int cor = 0;
		int CONT;
		
		do {
			CONT = 0;
			for (int index = 1; index < (Data.GetNdata()); index++) {
				resposta.Replace(RN.FeedForward(Data.GetIn(index)));
				for (int i = 0; i < resposta.Size(0); i++) {
					if (resposta.Get(i, 0) > maior) {
						maior = resposta.Get(i, 0);
						cor = i + 1;
					}
				}
				maior = 0;
				double out = Data.GetOut(index)[0];
				if (cor != out) {
					CONT = CONT + 1;// CONTA QUANTOS ERROS OCORREU EM QUANTO PASSAVA NO BANCO DE DADOS
					RN.Train(Data.GetIn(index), Dataout(index));

				}
			}

			System.out.println("ind: " + CONT);

		} while ((CONT > 2));
		RN.PrintPessos();

	}

	public static void main(String[] args) {
		Double FUCTION[][] = { { TYPE.SOFTMAX, TYPE.SOFTMAX, TYPE.SOFTMAX,TYPE.SOFTMAX, TYPE.SOFTMAX, TYPE.SOFTMAX,TYPE.SOFTMAX} };
		Using RedeNeural_RGB = new Using(3, 1, FUCTION);
		RedeNeural_RGB.Train();

	}

}
