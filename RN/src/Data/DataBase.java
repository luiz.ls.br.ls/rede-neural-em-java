
package Data;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class DataBase {

	private ArrayList<double[]> DataIn = new ArrayList<double[]>();
	private ArrayList<double[]> DataOut = new ArrayList<double[]>();
	private int Ninput, Noutput, Ndata;

	public DataBase(int Ninput, int Noutput) {
		Directory addressArchive = new Directory();
		this.Ninput = Ninput;
		this.Noutput = Noutput;
		
		try {
			Scanner entrada = new Scanner(addressArchive.arquivo);
			while (entrada.hasNext()) // hasNext > Ler arquivo até não ter linha vazia
			{
				try {
					String linha = entrada.nextLine();
					String[] vetor = linha.split(" ");
					double in[] = new double[this.Ninput];
					double out[] = new double[this.Noutput];

					for (int i = 0; i < this.Ninput; i++) {
						in[i] = Double.parseDouble(vetor[i]);
					}

					for (int i = this.Ninput; i < this.Ninput + this.Noutput; i++) {
						out[i - this.Ninput] = Double.parseDouble(vetor[i]);

					}

					DataIn.add(in);
					DataOut.add(out);

				} catch (NoSuchElementException excecaoElemento) {
					System.err.println("Arquivo com formato incorreto ");
					entrada.close();
					System.exit(1);
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("Arquivo não encontrado");
		}
		this.Ndata = DataIn.size();
	}

	public double[] GetIn(int index) {
		if ((index > 0) && (index <= this.Ndata))
			return DataIn.get(index - 1);
		else {
			System.err.println("Indice incorreto:");
			System.err.println("Dados de 1 a " + this.Ndata);
		}
		double[] e = { 0 / 0 };
		return (e);
	}
	
	public double[] GetOut(int index) {
		if ((index > 0) && (index <= this.Ndata))
			return DataOut.get(index - 1);
		else {
			System.err.println("Indice incorreto:");
			System.err.println("Dados de 1 a " + this.Ndata);
		}
		double[] e = { 0 };
		return (e);
	}
	public int GetNdata() {
		return Ndata;
		
	}

	public void PrintData() {
		for (int i = 1; i <= Ndata; i++) {
			System.out.print("I:"+i+" ");
		double in[]= GetIn(i);
		double out[]= GetOut(i);
		for (int j = 0; j < this.Ninput; j++) {
		System.out.print(""+in[j]+" ");
		}
		for (int k = 0; k < this.Noutput; k++) {
			System.out.print(""+out[k]+" ");
			}
		System.out.println("");
		}
		

	}

	
	
	public static void main(String[] args) {

		DataBase db = new DataBase( 3, 1);
		db.PrintData();
		
	
	}
	
	
}
