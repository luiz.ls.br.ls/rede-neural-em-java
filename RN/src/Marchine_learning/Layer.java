package Marchine_learning;

import java.util.ArrayList;

import Math.Matrix;
import Math.TYPE;
public class Layer {
	private int n_input;
	private int n_output;
	private double learning_rate = 0.1;
	public Matrix bias;
	public Matrix weights;
	private ArrayList<Double> FuncaoAtivacao = new ArrayList<Double>();
	// feedforward
	private Matrix input = new Matrix();
	private Matrix output = new Matrix();
	// backward
	private Matrix in_t = new Matrix();
	private Matrix out_D = new Matrix();// saida calculada derivada
	private Matrix grad = new Matrix();
	private Matrix deltaW = new Matrix();

	Layer(int n_input, ArrayList<Double> FuncaoAtv) {
		for (int i = 0; i < FuncaoAtv.size(); i++) {
			FuncaoAtivacao.add(FuncaoAtv.get(i));
		}
		this.n_input = n_input;// quantas entradas tem essa camada
		this.n_output = FuncaoAtivacao.size();// quantas saidas (ou nos) tem essa camada

		bias = new Matrix(this.n_output, 1); // bias que vai ser somado
		bias.Randomize();
		weights = new Matrix(this.n_output, this.n_input);// pesos que va ser ,ultiplicado pela entrada
		weights.Randomize();

	}

	public Matrix FeedForward(Matrix input) {

		this.input.Replace(input);
		this.output.Mult(this.weights, this.input);// pesos * entrada//produto vetorial
		this.output.Sum(this.output, this.bias);// (pesos * entrada)+bias
//		System.out.println("\\\\\\\\\\\\\\\\\\:" );
//		output.PrintTable();
		this.output.Aplicafunc(this.FuncaoAtivacao);// aplica a funçao de ativaçao nesse caso e a funçao sigmoid
//		output.PrintTable();
		return this.output;

	}

	public void Printfunc() {
		for (int i = 0; i < FuncaoAtivacao.size(); i++) {
			if (FuncaoAtivacao.get(i) == TYPE.SIGMOID)
				System.out.print(" SIGMOID \n");
			else if (FuncaoAtivacao.get(i) == TYPE.LeakyReLU)
				System.out.print(" BIPOLAR_SIGMOID \n");
			else if (FuncaoAtivacao.get(i) == TYPE.TANH)
				System.out.print(" TANH \n");
		}
		System.out.print("---------\n");
	}

	public void Backprpagation(Matrix erro) {
		if (this.FuncaoAtivacao.get(0) == TYPE.SOFTMAX)// ESSE E UM GEITO DE FAZER O CALCULO FUNCIONAR PARA A SOFTMAX
														// NAO SEI SE E O MEDO CORRETO OU IDEAL PARA MAIS INFORMAÇAO
														// PESQUISE SOBRE ENTROPIA CRUZADA COM SOFTMAX
			grad.Replace(erro);
		else {
			out_D.Replace(output);
			
			out_D.Deriva(this.FuncaoAtivacao);// derivada da sigmoid
			grad.Hadamard(erro, out_D);// grad=out_erro*outdesejado
		}
//		grad.PrintTable();
//		bias.PrintTable();
		grad.Mult(grad, learning_rate);// grad=grad*(o quanto vai corrigir)
		bias.Sum(bias, grad);// cottigindo os bias
		in_t.Transpose(input);// transpondo a entrada
		deltaW.Mult(grad, in_t);// correcao=grad*entrada_t//calculo do ajuste

		weights.Sum(weights, deltaW);// corrigindo os pessos

	}

	public static void main(String[] args) {

	}

}
